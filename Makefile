APP = app
.PHONY: all
all:run

.PHONY: build
build:
	$(MAKE) -C $(APP) build

.PHONY: run
run: build start-proxy
	 $(MAKE) -C $(APP) run

.PHONY: start-proxy
start-proxy:
	sudo $(MAKE) -C sidecar start

.PHONY: stop
stop:
	rm -r $(APP)/bin
	sudo $(MAKE) -C sidecar stop