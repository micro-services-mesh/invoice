package api

import (
	"net/http"

	"gitlab.com/snehaldangroshiya/invoice/model"

	"github.com/gorilla/mux"
)

// InitContacts func
func (api *API) InitInvoice(contacts *mux.Router) {
	contacts.Handle("", api.APIHandler(createInvoice)).Methods("POST")
}

func createInvoice(c *Context, w http.ResponseWriter, r *http.Request) {
	var contact = &model.Invoice{}
	contactModel, modelErr := contact.FromJson(r.Body)
	if modelErr != nil {
		c.Err = model.NewAppError("api.createInvoice", "json.conversation.app_error", nil, "id="+modelErr.Error(), http.StatusBadRequest)
		return
	}

	props, createUserErr := c.App.CreateInvoice(contactModel.(*model.Invoice))
	if createUserErr != nil {
		c.Err = createUserErr
		return
	}

	w.WriteHeader(http.StatusCreated)
	result, jsonErr := props.ToJson(props)
	if jsonErr != nil {
		c.Err = model.NewAppError("api.createInvoice", "json.conversation.app_error", nil, "id="+jsonErr.Error(), http.StatusBadRequest)
		return
	}
	w.Write(result)
}
