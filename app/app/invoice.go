package app

import (
	"gitlab.com/snehaldangroshiya/invoice/model"
	"gitlab.com/snehaldangroshiya/invoice/utils"
)

const tableContacts = "invoice"

// CreateInvoice func
func (app *App) CreateInvoice(contact *model.Invoice) (*model.Invoice, *model.AppError) {
	result := <-app.store.Invoice().Create(tableContacts, contact)
	if result.Err != nil {
		app.logger.Errorln(utils.T("store.message_store.create.ipm_subtree.app_error"))
		return nil, result.Err
	}
	return result.Data.(*model.Invoice), nil
}
