package store

import (
	"net/http"

	"gitlab.com/snehaldangroshiya/invoice/model"
)

// Invoice struct
type Invoice struct {
	transport
}

// NewContactStore func
func NewInvoiceStore(transport transport) IMAPIInvoice {
	return &Invoice{transport}
}

func (c *Invoice) Create(collection string, contact *model.Invoice) Channel {
	storeChannel := make(Channel, 1)
	go func() {
		result := Result{}
		c := c.connect()

		contact.PreSave()
		p, err := contact.ToBson(contact)
		if err != nil {
			result.Err = model.NewAppError("DBItemStore.Create", "store.db_item.create.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		} else if err := c.CreateDocument(collection, p); err != nil {
			result.Err = model.NewAppError("DBItemStore.Create", "store.db_item.create.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		}
		result.Data = contact
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}
