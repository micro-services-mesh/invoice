package store

import "github.com/snehal1112/transport/client"

// SupplierStores struct
type SupplierStores struct {
	contactStore IMAPIInvoice
}

// Supplier struct
type Supplier struct {
	stores    SupplierStores
	transport config
}

// NewSupplier create new instance of Supplier.
func NewSupplier() *Supplier {
	supplier := &Supplier{}

	supplier.stores.contactStore = NewInvoiceStore(supplier)

	return supplier
}

// Contact function returns the user store object.
func (s *Supplier) Invoice() IMAPIInvoice {
	return s.stores.contactStore
}

func (s *Supplier) connect() *client.Connect {
	return s.transport.connect()
}
