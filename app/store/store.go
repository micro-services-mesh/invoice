package store

import "gitlab.com/snehaldangroshiya/invoice/model"

// Result struct
type Result struct {
	Data interface{}
	Err  *model.AppError
}

// Channel type
type Channel chan Result

// Store interface
type Store interface {
	Invoice() IMAPIInvoice
}

type IMAPIInvoice interface {
	Create(collection string, contactProps *model.Invoice) Channel
}

type IMAPIExtension interface {
	Create()
	Get()
	Update()
	Delete()
}
