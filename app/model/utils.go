package model

import (
	"bytes"
	"encoding/base32"
	"encoding/json"

	"github.com/pborman/uuid"
)

var encoding = base32.NewEncoding("ybndrfg8ejkmcpqxot1uwisza345h769")

// AppError struct
type AppError struct {
	ID            string `json:"id"`
	Message       string `json:"message"`               // Message to be display to the end user without debugging information
	DetailedError string `json:"detailed_error"`        // Internal error string to help the developer
	RequestID     string `json:"request_id,omitempty"`  // The RequestId that's also set in the header
	StatusCode    int    `json:"status_code,omitempty"` // The http status code
	Where         string `json:"-"`                     // The function where it happened in the form of Struct.Func
	IsOAuth       bool   `json:"is_oauth,omitempty"`    // Whether the error is OAuth specific
	params        map[string]interface{}
}

// NewID is a globally unique identifier.  It is a [A-Z0-9] string 26
// characters long.  It is a UUID version 4 Guid that is zbased32 encoded
// with the padding stripped off.
func NewID() string {
	var b bytes.Buffer
	encoder := base32.NewEncoder(encoding, &b)
	encoder.Write(uuid.NewRandom())
	encoder.Close()
	b.Truncate(26) // removes the '==' padding
	return b.String()
}

// TranslateFunc type
type TranslateFunc func(translationID string, args ...interface{}) string

// NewAppError func
func NewAppError(where string, ID string, params map[string]interface{}, details string, status int) *AppError {
	return &AppError{
		ID:            ID,
		params:        params,
		Message:       ID,
		Where:         where,
		DetailedError: details,
		StatusCode:    status,
		IsOAuth:       false,
	}
}

// Translate function
func (er *AppError) Translate(T TranslateFunc) {
	if er.params == nil {
		er.Message = T(er.ID)
	} else {
		er.Message = T(er.ID, er.params)
	}
}

// ToJSON function
func (er *AppError) ToJSON() string {
	b, err := json.MarshalIndent(er, "", "    ")
	if err != nil {
		return ""
	}

	return string(b)
}
